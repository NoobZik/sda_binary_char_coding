/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 14:15:03 by NoobZik           #+#    #+#             */
/*   Updated: 2018/12/21 14:43:02 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>

/**
 * Disclaimer : As requested on the assignement, Structures and prototypes
 * (supposed) should not be touched regardless the reason. Only one file (.c,
 * this one) should be sent.
 *
 * This assignement cannot meet the guidelines due to the requirements
 * (Check on Docs folder, "projet-18.pdf")
 * - Fully english code (But comments are in english)
 * - inline keywords for single simple function operation
 *
 * It is assumed that this assignement must handle all character in the ASCII
 * table.
 */

// Opaque struct (Tree)
typedef struct        node_s {
  struct node_s       *fg;
  struct node_s       *fd;
  char                symbol;
  int                 freq;
}                     node;

// Opaque struct (Heap)
typedef struct        tas_s {
  int                 m;
  int                 n;
  node                **tab;
}                     tas;


/* Opaque assignement prototypes.*/

node*           creer_node          (char, int);
tas*            inic_tas            (int);
/* inline */int est_vide_tas        (tas *);
void            inserer_tas         (tas *, node *);
tas*            saisie_alphabet     (void);
node*           supprimer_tas       (tas *);
node*           creer_arbre         (tas *);
/* inline */int est_feuille         (node *);
void            imprimer_arbre      (node *);
void            imprimer_codes      (node *, char *, int);
void            supprimer_arbre     (node *);
void            liberer_memoire_tas (tas *);

/* Personal prototypes helpers */
int             root                (int);
int             right               (int);
int             left                (int);

int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;
  int i = -1;
  puts("Warning : Follow and read the instruction carefully. Any typing error"
  " is considered an assertion violation, leading to immediate termination"
  " of the process\n\n");
  tas *buffer;
  node *tree;
  char output[500000];
  buffer = saisie_alphabet();

  printf("Heap content : ");
  while (++i < buffer->n) {
    printf("%d ", buffer->tab[i]->freq);
  }
  printf("\n");
  assert(buffer);
  assert((tree = creer_arbre(buffer)));
  imprimer_codes(tree, output, 0);
  supprimer_arbre(tree);
  liberer_memoire_tas(buffer);
  return 0;
}


/**
 * Create (Allocate) a node with the given char and n value.
 *
 * Complexity : 0(1)
 *
 * @param  c A single character
 * @param  n Frequency of the character c.
 * @return   Fully initialized node.
 */
node *creer_node (char c, int n) {
  node *res = 0;
  if(!(res = malloc(sizeof(node))))
    return NULL;
  res->fg = NULL;
  res->fd = NULL;
  res->symbol = c;
  res->freq = n;
  return res;
}

/**
 * Create a empty fixed size of the heap (int n).
 *
 * Complexity : O(1)
 *
 * @param  n Size of the heap
 * @return   Fully initialized heap
 */
tas *inic_tas (int n) {
  tas *res = 0;
  if(!(res = malloc(sizeof(tas))))
    return NULL;
  res->n = 0;
  res->m = n;
  if (!(res->tab = malloc(sizeof(node *)* (size_t) n))) {
    free(res);
    return NULL;
  }
  return res;
}

/**
 * Check if the heap is empty or note.
 * This function should have a bool return type !
 *
 * Complexity : O(1)
 *
 * @param  h A pointer to the heap
 * @return   1 = true, 0 = false.
 */
/* inline */int est_vide_tas (tas *h) {
  return (!h->n) ? 1 : 0;
}

/**
 * Insert a node into the heap (min rules for the sift part)
 *
 * Complexity : O(log(n)).
 *
 * @param t Pointer to heap
 * @param n Pointer to nodes
 */
void inserer_tas (tas *h, node *n) {
  assert(h);
  assert(n);
  node *tmp;

  if (h->n == 0) {
    h->tab[0] = n;
    h->n += 1;
    return;
  }

  int i = h->n;
  int p = i;

  assert(h->m >= (h->n)+1);
  h->tab[h->n] = n;
  h->n += 1;

  // Sift up if needed

  while ((h->tab[root(i)]->freq > h->tab[p]->freq)) {
    tmp = h->tab[root(i)];
    h->tab[root(i)] = h->tab[p];
    h->tab[p] = tmp;
//    swap(&h->tab[root(i)], &h->tab[p]);
    p = root(i);
    i = p;
  }
}

/**
 * Ask the user to type the ammount of caracter to be read.
 * Create a heap with a fixed size (number of caracter to be read)
 * Ask the user to type a char at a time until it reach the fixed size of the
 * heap.
 *
 * Complexity : O(N) (Depends on the user)
 *
 * @return Heap
 */
tas *saisie_alphabet () {
  tas *res = 0;
  int i = -1;
  int n;
  int f;
  char c;
  char str[100];

  printf("Type the amount of alphabetic letter (int): ");
  fgets(str, sizeof(str), stdin);
  strtok(str,"\n");
  n = atoi(str);

  assert((res = inic_tas(n)) != NULL);
  while (++i < n) {
    puts("Great, now type any valid caracter (ASCII)");
    printf("Only one caracter at a time please (char): ");
    fgets(str, sizeof(str), stdin);
    strtok(str,"\n");
    c = str[0];

    printf("Now type it frequency (number): ");
    fgets(str, sizeof(str), stdin);
    strtok(str,"\n");
    f = atoi(str);

    inserer_tas(res, creer_node(c,f));
    assert(res->tab[0]);
  }
  return res;
}

/**
 * Extract the lowest min heap key. The lin key is located at the root
 * Sift left all nodes once extracted
 *
 * Complexity : O(log(n)).
 * @param  t Pointer to heap
 * @return   Extracted node
 */
node* supprimer_tas (tas *h) {
  assert(h);
  assert(h->tab[0]);
  node *max = h->tab[0];
  int i = 0;
  int cont = 1;
  int greatest;
  node *tmp;

  if (h->n == 1) {
    h->n -= 1;
    return max;
  }

  h->n--;
  h->tab[0] = h->tab[h->n];

  while (cont) {
    if (right(i) > h->n) {
      cont = 0;
    }
    else if (right(i) == h->n) {
      if (h->tab[i]->freq > h->tab[left(i)]->freq) {
        tmp = h->tab[i];
        h->tab[i] = h->tab[left(i)];
        h->tab[left(i)] = tmp;
      }
      cont = 0;
    }
    else {
      if (h->tab[i]->freq > h->tab[left(i)]->freq ||
          h->tab[i]->freq > h->tab[right(i)]->freq) {
        greatest = (h->tab[left(i)]->freq < h->tab[right(i)]->freq) ? left(i) : right(i);

//        swap(h->tab[i], h->tab[greatest]);
        tmp = h->tab[i];
        h->tab[i] = h->tab[greatest];
        h->tab[greatest] = tmp;

        i = greatest;
      }
      else {
       cont = 0;
      }
    }
  }
  return max;
}
/**
 * Generate a tree according to the heap.
 *
 * Complexity : O(N) (Size of the heap, before editing).
 *
 * @param  b A heap
 * @return   A generated tree located at the top of the heap
 */
node* creer_arbre (tas *h) {
  assert(h);
  node *z;
  node *x;
  node *y;

  int n = h->n;
  int i = -1;
  int freq;

  while (++i < n) {
      if (h->n == 1) {
        x = supprimer_tas(h);
        z = creer_node('\0', x->freq);
        z->fg = x;
        inserer_tas(h, z);
        break;
      }
      x = supprimer_tas(h);
      y = supprimer_tas(h);


      if (x && y)
        freq = x->freq + y->freq;
      if (x && !y)
        freq = x->freq;
      if (!x && y)
        freq = y->freq;
      z = creer_node('\0', freq);
      assert(z);
      z->fg = x;
      z->fd = y;

      inserer_tas(h,z);
  }
  assert(h->tab[0]);
  return supprimer_tas(h);
}

/**
 * Return an interger value according to the node to be a leaf of not.
 *
 * This function shoudle use bool return type.
 *
 * Complexity : O(1)
 *
 * @param  n A Node
 * @return   1 = true, 0 = false
 */
/* inline */int est_feuille (node *n) {
  assert(n);
  return (!n->fg && !n->fd) ? 1 : 0;
}

/**
 * Infix printing tree recursively
 *
 * Complexity : O(N) (Size of the tree)
 *
 * @param b Pointer to the tree
 */
void imprimer_arbre (node *b) {
  if (!b) return;
  imprimer_arbre(b->fg);
  printf("%c:%d ", b->symbol, b->freq);
  imprimer_arbre(b->fd);
}

/**
 * Print the code formatted of the tree.
 * This function must be called with a fixed size leaf+1 of the tree.
 * Also it must be called with i = 0;
 *
 * Base case is the current node is a leaf, print the string.
 *
 * THe other case are ;
 * - If the left node exist, put 0 at i position of the string.
 * - If the right node exist, put 1 at i position of the string.
 * After both of these case, add 1 at i on the recursuve call of either node.
 *
 * Complexity : O(N) (Size of the tree)
 *
 * @param b Tree
 * @param c String output with a allocated size
 * @param i String position
 */
void imprimer_codes (node *b, char *c, int i) {
  assert(c);
  if (!b) return;

  if (est_feuille(b)) {
    c[i] = '\0';
    printf("%s:%c\n",c,b->symbol);
  }

  if (b->fg) {
    c[i] = '0';
    imprimer_codes(b->fg,c,i+1);
  }

  if (b->fd) {
    c[i] = '1';
    imprimer_codes(b->fd,c,i+1);
  }
}

/**
 * Frees the allocated memory of the given Tree.
 * @param b A node
 */
void supprimer_arbre (node *b) {
  if (!b) return;
  supprimer_arbre(b->fg);
  supprimer_arbre(b->fd);
  free(b);
}

/**
 * Free the contents of the heap then free itself.
 * @param t A pointer to a heap
 */
void liberer_memoire_tas (tas *h) {
  assert(h);
  int i = -1;
  while (++i < h->n)
    free(h->tab[i]);
  free(h->tab);
  free(h);
}

/*****************************************************************************/
/*                                                                           */
/*                                                                           */
/*                             Helpers                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/



/**
 * Return the root location value of the heap
 * @param  i Current index caller
 * @return   Root position result
 */
int root (int i) {
  return (int) floor((i-1)/2);
}


/**
 * Return the root location value of the heap
 * @param  i Current index caller
 * @return   Root position result
 */
int left (int i) {
  return i*2+1;
}


/**
 * Return the root location value of the heap
 * @param  i Current index caller
 * @return   Root position result
 */
int right (int i) {
  return i*2+2;
}
