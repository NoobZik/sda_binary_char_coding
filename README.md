# Rapport de Projet SDA : binary_coding_char #

| NOM Prénom     | Numéro étudiant |
| :------------- | :-------------- |
| SHEIKH Rakib   | 11502605        |


Lien vers le bitbucket [Here](https://bitbucket.org/NoobZik/sda_binary_char_coding/).

### Instructions pour faire marcher un fichier C ? ###

1.  tappez
```
$ gcc main.c -O0
```
Note : Pas de warnings - Pas d'erreurs, donc pas besoin de -Wall (De toute façon c'est pour les faibles).
Tant que vous y êtes, essayez avec cette commande :

```
$ gcc main.c -O0 -Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations -Wredundant-decls -Wnested-externs -Winline -Wno-long-long -Wuninitialized -Wconversion -Wstrict-prototypes
```
2.  tappez
```
$ ./a.out
```

3.  Suivez les instructions
4.  ???
5.  profit

### Codage du symbole ###

Bon déjà il faut d'abord demander à l'utilisateur les symboles. Le programme ne va pas s'amuser à chercher des symboles non plus. Pour ça, on demande tout simplement le nombre de caractères à mettre dans le tas. Ensuite, on demande de saisir les caractères et leur fréquence lorsque c'est demandé. Tout ceci est inséré dans un HEAP-MIN.

Le codage du symbole reste assez intuitif, il suffit d'appliquer bêtement l'algorithme suivant proposé pour la génération d'un arbre binaire.

```
On considère C, un heap déjà initialisé avec ses noeuds
Construction-Arbre-Codage (C) {
  n = tailleDuHeap(C)
  Tant que (i < n) faire {
    z = creer_node();
    x = Extraire_min(F);
    y = Extraire_min(F);
    z->gauche = x;
    z->droit = y;
    f[z] = f[x] + f[y];
    Inserer_tas(F,z);
  }
  Retourner Extraire_min(F);
}
```

Il suffit de parcourir l'arbre en infix pour avoir le codage d'un symbole. Si on parcourt à gauche, on écrit 1 dans le buffer du résultat (0 respectivement pour la droite).
Si on se trouve sur le noeud actuel possédant un symbole, alors on écrit le caractère de fin de chaine '\0' et on affiche le buffer.

N'empêche, j'ai l'impression de copier le sujet mais c'est demandé comment marche la fonction qui détermine les codes du symbole. Mon programme fait exactement ce qui est demandé. C'est bête...

### Pourquoi right(int), left(int) et root(int) ? ###

**Réponse** : Parce que j'ai la flemme d'écrire des calcules à chaque fois.

### Analyse de complexité ###

En bref :
-   La création de node, heap est en temps constant, donc O(1)
-   L'insertion d'un heap se fait en O(log(n)).
-   La saisie de l'alphabet se fait en O(n)
-   La suppression d'un tas : O(log(n))
-   La création d'un arbre binaire se fait en O(n) (Taille du Heap)
-   Les fonctions d'accès direct (estFeuille) se fait en O(1)
-   L'impression d'un code et d'un arbre se fait logiquement en O(n)
-   La suppression d'un arbre et d'un heap (libererMemoireTas) se font aussi en O(n).

Les fonctions additionnels (root, left et right) se font en O(1).

Etant donnée que dans le main, il y a l'affichage du contenu du heap O(n), on a au final :

O(n + n.log(n)) + O(n) + O(n + n (3log(n))) + O(n) + O(n) + O(n) + O(n)

O(5n) + O(n + n.log(n)) +O(n + n (3log(n)))

Programme = O(n).
