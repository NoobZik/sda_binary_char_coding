/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 22:55:24 by NoobZik           #+#    #+#             */
/*   Updated: 2018/11/24 23:49:21 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct {
  char c;
  int f;
} node;

int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;
  int i = 0;
  int n;
  int f;
  char str[100];
  char c;
//  char *str;
  node *test;

  printf("Type the amount of alphabetic letter (int): ");
  fgets(str, sizeof(str), stdin);
  strtok(str,"\n");
  n = atoi(str);

  if((test = malloc(sizeof(node)* (size_t) n)) == 0)
    return -1;

  while (i < n) {
    puts("Great, now type any valid caracter (ASCII)");
    printf("Only one caracter at a time please (char): ");
    fgets(str, sizeof(str), stdin);
    strtok(str,"\n");
    c = str[0];
    /** A partir de cette ligne, le buffer ne marche pas. Elle affiche le puts
     *  mais n'attends pas que le mec tappe le truc, et saute a la prochaine
     * itération de la boucle
     */
    printf("Now type it frequency (number): ");
    fgets(str, sizeof(str), stdin);
    strtok(str,"\n");
    f = atoi(str);
    //inserer_tas(res, creer_node(*str,f));
    test[i].c = c;
    test[i].f = f;
    printf("Now remaining : %d char\n", n-(i+1));
    i++;
  }

  i = -1;
  while (++i < n) {
    printf("%c:%d ", test[i].c, test[i].f);
  }
  printf("\n");
  free(test);
  return 0;
}
